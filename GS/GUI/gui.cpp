#include "GUI.h" 

int GUI::initWindow() {
	int returnVal = 0;
	// Initialise GLFW
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		getchar();
		returnVal = -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	//GLFWmonitor *monitor = glfwGetPrimaryMonitor();
	//const GLFWvidmode* mode = glfwGetVideoMode(monitor);
	//FULL SCREEN
	//window = glfwCreateWindow(mode->width, mode->height, "Schwifty", monitor, NULL);
	window = glfwCreateWindow(1000, 800, "Schwifty", NULL, NULL);

	if(window == NULL ){
		fprintf( stderr, "Failed to open GLFW window.\n" );
		getchar();
		glfwTerminate();
		returnVal = -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		getchar();
		glfwTerminate();
		returnVal = -1;
	}

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS); 

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);

	// Hide the mouse..
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	return returnVal;
}

//
//	MAIN RENDER LOOP
//
int GUI::Render() {
	int returnVal = 0;
	if( glfwGetKey(window, GLFW_KEY_ESCAPE ) == GLFW_PRESS && glfwWindowShouldClose(window) == 0 ) {
		GUI::cleanUp();
		returnVal = -1;
	} else {
		// Clear the screen
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// Use our shader
		glUseProgram(this->programID);

		//	Handle the controls!
		GUI::getControl()->computeMatricesFromWindowInputs(window);
		mat4 ProjectionMatrix = GUI::getControl()->getProjectionMatrix();
		mat4 ViewMatrix = GUI::getControl()->getViewMatrix();
		mat4 ModelMatrix = mat4(1.0);
		mat4 MVP = ProjectionMatrix * ViewMatrix * ModelMatrix;
		// Send our transformation to the currently bound shader, 
		// in the "MVP" uniform
		glUniformMatrix4fv(this->MatrixID, 1, GL_FALSE, &MVP[0][0]);


		// DO SOME Stuff HERE
		
		//
		//	FIGURE OUT HOW THE BELOW MAGIC RENDERS THE VERTEX BUFFER...
		//
		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, this->vertexbuffer);
		glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,0, (void*)0);

		// Draw the triangle !
		glDrawArrays(GL_TRIANGLES, 0, this->vertices.size() );


		// Look into the below two lines to see if they are needed...
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	return returnVal;
}

void GUI::cleanUp() {
	// Cleanup VBO and shader
	glDeleteBuffers(1, &this->vertexbuffer);
//	glDeleteBuffers(1, &uvbuffer);
//	glDeleteTextures(1, &Texture);

	glDeleteProgram(this->programID);
	glDeleteVertexArrays(1, &this->VertexArrayID);

	//CLOSE THE windows.. 
	glfwDestroyWindow(window);
	glfwTerminate();
}

bool GUI::LoadSceneTest () {
	glGenVertexArrays(1, &this->VertexArrayID);
	glBindVertexArray(this->VertexArrayID);

	// Create and compile our GLSL program from the shaders
	this->programID = LoadShaders( "./lib/common/TransformVertexShader.vertexshader", "./lib/common/TextureFragmentShader.fragmentshader" );

	// Get a handle for our "MVP" uniform
	this->MatrixID = glGetUniformLocation(programID, "MVP");

	// Read our .obj file
	bool res = loadOBJ("./assets/cube.obj", this->vertices, this->uvs, this->normals);
	if(res){
		// Load it into a VBO

		glGenBuffers(1, &this->vertexbuffer);
		glBindBuffer(GL_ARRAY_BUFFER, this->vertexbuffer);
		glBufferData(GL_ARRAY_BUFFER, this->vertices.size() * sizeof(vec3), &this->vertices[0], GL_STATIC_DRAW);

		//GLuint uvbuffer;
		//glGenBuffers(1, &uvbuffer);
		//glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
		//glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(vec2), &uvs[0], GL_STATIC_DRAW);
	}
	return res;
}