#ifndef GUI_CLASS
#define GUI_CLASS

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>

#include "../../lib/gl/glew.h"
#include "../../lib/GLFW/glfw3.h"
#include "../../lib/glm/glm.hpp"

#include "../../lib/common/shader.hpp"
#include "../../lib/common/texture.hpp"
#include "../../lib/common/objloader.hpp"
#include "../../lib/common/controls.hpp"

using namespace glm;
using namespace std;

class GUI
{
    public:
        GUI(){};
        ~GUI(){this->Destroy();};

        GLFWwindow* window;
        
        GLuint VertexArrayID;
        GLuint programID;
	    GLuint MatrixID;
        GLuint vertexbuffer;

        vector<vec3> vertices;
        vector<vec2> uvs;  // Won't be used at the moment.
        vector<vec3> normals; // Won't be used at the moment.

        void Destroy() {
            this->window = NULL;
            this->cntrl = NULL;
        };

        int init(){
            this->ARE_WE_RUNNING = initWindow();
            return this->ARE_WE_RUNNING;
        };

        bool LoadSceneTest();

        Controls* getControl(){return this->cntrl;};
        int Render();
        void setControl(Controls *ctl){this->cntrl = ctl;};
        void cleanUp();

    private:
        int initWindow();
        int ARE_WE_RUNNING;
        Controls *cntrl;
}; 

#endif