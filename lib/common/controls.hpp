#ifndef CONTROLS_HPP
#define CONTROLS_HPP

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#include "../../lib/GLFW/glfw3.h"
#include "../../lib/glm/glm.hpp"
#include "../../lib/glm/gtc/matrix_transform.hpp"
using namespace glm;

class Controls
{
    public:
        Controls(){};
        ~Controls(){};

        // Initial position : on +Z
        vec3 position; 
        // Initial horizontal angle : toward -Z
        float horizontalAngle;
        // Initial vertical angle : none
        float verticalAngle;
        // Initial Field of View
        float initialFoV;
        float speed; // 3 units / second
        float mouseSpeed;

        void init() {
            this->position = vec3( 0, 0, 5 ); 
            this->horizontalAngle = 3.14f;
            this->verticalAngle = 0.0f;
            this->initialFoV = 45.0f;
            this->speed = 3.0f; 
            this->mouseSpeed = 0.00025f;
        }
        void computeMatricesFromWindowInputs(GLFWwindow* window);
        mat4 getViewMatrix(){return this->ViewMatrix;}
        mat4 getProjectionMatrix(){return this->ProjectionMatrix;}
    private:
        mat4 ViewMatrix;
        mat4 ProjectionMatrix;
};
#endif