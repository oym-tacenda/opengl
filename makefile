all: app

app: main.o gui.o controls.o shader.o objLoader.o
	g++ -o app main.o gui.o controls.o shader.o objLoader.o -I/usr/local/include -L/usr/local/lib -lglfw3 ./lib/libGLEW.a -framework Cocoa -framework OpenGL -framework IOKit -framework CoreFoundation -framework CoreVideo -w

main.o: main.cpp
	g++ -c main.cpp

gui.o: ./GS/GUI/gui.cpp ./GS/GUI/GUI.h
	g++ -c ./GS/GUI/gui.cpp
	
controls.o: ./lib/common/controls.cpp ./lib/common/Controls.hpp
	g++ -c ./lib/common/controls.cpp

shader.o: ./lib/common/shader.cpp ./lib/common/shader.hpp
	g++ -c ./lib/common/shader.cpp

objLoader.o: ./lib/common/objloader.cpp ./lib/common/objloader.hpp
	g++ -c ./lib/common/objloader.cpp

clean:
	rm *.o app