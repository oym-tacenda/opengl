#include <iostream>
#include "./GS/GUI/GUI.h"

using namespace std;

int main()
{
  int GameIsRunning = 0;

  GUI *myGUI = new GUI();
  Controls *ctrl = new Controls();
  myGUI->init();
	ctrl->init();
	myGUI->setControl(ctrl);

  int res = myGUI->LoadSceneTest();
  do {
    GameIsRunning = myGUI->Render();
  }while (GameIsRunning != -1);
  
  myGUI->Destroy();
  myGUI = NULL;
  ctrl = NULL;
  return 0;
}
